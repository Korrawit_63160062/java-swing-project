/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.korrawit.javaswing;

/**
 *
 * @author DELL
 */
import javax.swing.*;

public class JavaJProgressBar_1 extends JFrame {

    JProgressBar jb;
    int i = 0, num = 0;

    JavaJProgressBar_1() {
        jb = new JProgressBar(0, 2000);
        jb.setBounds(40, 40, 160, 30);
        jb.setValue(0);
        jb.setStringPainted(true);
        add(jb);
        setSize(250, 150);
        setLayout(null);
    }

    public void iterate() {
        while (i <= 2000) {
            jb.setValue(i);
            i = i + 20;
            try {
                Thread.sleep(150);
            } catch (Exception e) {
            }
        }
    }

    public static void main(String[] args) {
        JavaJProgressBar_1 m = new JavaJProgressBar_1();
        m.setVisible(true);
        m.iterate();
    }
}
